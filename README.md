# umi project

## Getting Started

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start

# login
http://localhost:8000/login
# search
http://localhost:8000/login
# tags
http://localhost:8000/tags
```
