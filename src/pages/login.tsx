import React from 'react'
import { Input, Button, Form, Row } from 'antd'
import { UserOutlined, KeyOutlined } from '@ant-design/icons'
import styles from './login.less'

const LoginPage = () => {
  const onFinish = values => {
    // TODO: 登陆后的逻辑
    console.log('submit', values)
  }
  return (
    <Form className={styles.container} onFinish={onFinish}>
      <Form.Item
        name="username"
        rules={[{ required: true, message: '用户名不能为空' }]}
      >
        <Input
          prefix={<UserOutlined />}
          size="large"
          placeholder="请输入用户名"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: '请输入密码' }]}
      >
        <Input.Password
          prefix={<KeyOutlined />}
          size="large"
          placeholder="请输入密码"
        />
      </Form.Item>
      <Form.Item>
        <Row align="middle" justify="space-around">
          <Button size="large">注册</Button>
          <Button type="primary" size="large" htmlType="submit">
            登陆
          </Button>
        </Row>
      </Form.Item>
    </Form>
  )
}

LoginPage.layout = {
  hideMenu: true,
  hideNav: true,
  name: '登陆',
}

export default LoginPage
