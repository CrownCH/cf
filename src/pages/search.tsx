import React from 'react'
import { Card, Input, List, Row, Col, Typography } from 'antd'
import styles from './search.less'

const data = [
  [
    <a href="https://baidu.com">测试文字</a>,
    'Racing car sprays burning fuel into crowd.',
    '多行文字',
    '多行文字',
  ],
  'Japanese princess to wed commoner.',
  'Australian walks 100km after outback crash.',
  'Man charged over missing wedding girl.',
  'Los Angeles battles huge wildfires.',
]

const SearchPage = () => {
  const onSearch = (value, event) => {
    // TODO: search
    console.log('search', value, event)
  }

  const renderItem = item => (
    <List.Item>
      {Array.isArray(item) ? (
        <div className={styles.flexColumn}>
          {item.map((v, i) => (
            <Row key={i}>{v}</Row>
          ))}
        </div>
      ) : (
        item
      )}
    </List.Item>
  )

  return (
    <Card
      title={
        <Input.Search size="large" enterButton="搜索" onSearch={onSearch} />
      }
    >
      <Row>
        <Col span={11}>
          <List
            dataSource={data}
            renderItem={renderItem}
            pagination={{ defaultCurrent: 1, defaultPageSize: 5, total: 50 }}
          />
        </Col>
        <Col span={11} push={2}>
          <Typography.Title level={4}>推荐观看</Typography.Title>
          <List dataSource={data} renderItem={renderItem} />
        </Col>
      </Row>
    </Card>
  )
}

SearchPage.menu = {
  name: '搜索',
}

export default SearchPage
